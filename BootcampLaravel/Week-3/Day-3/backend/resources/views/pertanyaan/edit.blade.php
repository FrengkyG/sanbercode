@extends('layouts.master')

@section('content')
   <div class="ml-3 mt-3 mr-3">
      <div class="card card-primary">
         <div class="card-header">
         <h3 class="card-title">Edit Pertanyaan {{$post->id}}</h3>
         </div>
         <!-- /.card-header -->
         <!-- form start -->
         <form role="form" action="/pertanyaan/{{$post->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
               <div class="form-group">
               <label for="judul">Judul</label>
               <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', $post->judul)}}" placeholder="Masukan Judul" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
                  @error('judul')
                     <div class="alert alert-danger">{{ $message }}</div>
                  @enderror   
               </div>
               <div class="form-group">
               <label for="isi">Isi</label>
               <input type="Text" class="form-control" id="isi" name="isi" value="{{ old('isi', $post->isi)}}"placeholder="Masukan isi" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">
                  @error('isi')
                     <div class="alert alert-danger">{{ $message }}</div>
                  @enderror 
               </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
               <button type="submit" class="btn btn-primary">Edit</button>
            </div>
         </form>
      </div>
   </div>
@endsection