<!DOCTYPE html>
<html>
<head>
   <title>SanberBook - Sign Up</title>
</head>
<body>
   <h1>Buat Account Baru</h1>
   <h2>Sign Up Form</h2>
   <form method="post" action="/welcome">
      @csrf
      <label for="first_name">First name:</label>
      <br><br> 
      <input type="text" name="first_name">
      <br><br>

      <label for="last_name">Last name:</label>
      <br><br>
      <input type="text" name="last_name">
      <br><br>

      <label>Gender:</label>
      <br><br>
      <input type="radio" name="gender" value="0">Male <br>
      <input type="radio" name="gender" value="1">Female <br>
      <input type="radio" name="gender" value="2">Other <br>
      <br>

      <label>Nationality</label>
      <br><br>
      <select>
         <option>Indonesian</option>
         <option>Malaysian</option>
         <option>Singaporean</option>
         <option>Australian</option>
      </select>
      <br><br>

      <label>Language Spoken:</label>
      <br><br>
      <input type="checkbox" name="lang" value="0">Bahasa Indonesia <br>
      <input type="checkbox" name="lang" value="1">English <br>
      <input type="checkbox" name="lang" value="2">Other <br>
      <br><br>

      <label>Bio:</label>
      <br><br>
      <textarea cols="30" rows="10"></textarea>
      <br>
      
      <button type="submit">Sign Up</button>
   </form>
</body>
</html>