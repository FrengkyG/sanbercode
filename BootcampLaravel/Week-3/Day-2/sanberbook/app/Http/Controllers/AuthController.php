<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{   
    public function show_register(){
        return view('register');
    }
    public function register(Request $request){
        $nama = $request['first_name'] . " " . $request['last_name'];
        return view('welcome', compact('nama'));
    }
}
