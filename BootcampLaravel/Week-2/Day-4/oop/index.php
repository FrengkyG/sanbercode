<?php

require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

$sheep = new animal("shaun");

echo $sheep->name . "<br>"; // "shaun"
echo $sheep->legs . "<br>"; // 2
echo $sheep->cold_blooded . "<br>"; // false
var_dump($sheep->cold_blooded);
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

echo "<h3> Menggunakan method get </h3>";
echo "<br>" . $sheep->get_name() . "<br>";
echo $sheep->get_legs() . "<br>";
var_dump($sheep->get_cold_blooded());

echo "<br>";
echo "<h3>Release 1</h3>";

$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"

?>