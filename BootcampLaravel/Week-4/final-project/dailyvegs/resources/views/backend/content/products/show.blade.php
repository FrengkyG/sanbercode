@extends('backend.layouts.master')

@section('content')
<div class="card shadow mb-4">
   <div class="card-header py-3">
      <h3 class="m-0 font-weight-bold text-primary">{{ $product->product_name }}</h3>
   </div>
   <div class="card-body">
      <img class="img-fluid rounded mx-auto d-block" style="width:200px; height:200px"src="{{ asset('/vegefoods/images/product-1.jpg') }}" alt="Colorlib Template">
         <div class="overlay"></div>
       <div class="text py-3 pb-4 px-3 text-center">
         <h3>{{ $product->product_name }}</h3>
         <div class="d-flex">
           <div class="pricing mx-auto">
           <p class="price"><span>Rp {{ $product->price }}</span></p>
           </div>
         </div>
       </div>
   </div>
</div>
@endsection