@extends('backend.layouts.master')

@section('content')
<div class="card shadow mb-4">
   <div class="card-header py-3">
     <h3 class="m-0 font-weight-bold text-primary">Order List</h3>
   </div>
   <div class="card-body">
      @if(session('success'))
      <div class="alert alert-success" id="popup-success">
         {{ session('success')}}
      </div>
   @endif
   {{-- <a href="{{ route('products.create') }}" class="btn btn-primary btn-icon-split mb-2">
      <span class="icon text-white-50">
         <i class="fas fa-plus-circle"></i>
      </span>
      <span class="text">Create New Product</span>
    </a> --}}
   
   <table class="table table-bordered">
      <thead>
         <tr>
            <th style="width: 10px">#</th>
            <th>Order ID</th>
            <th>Name</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Status</th>
            <th style="width: 40px">Actions</th>
         </tr>
      </thead>
      <tbody>
         @forelse ($orders as $key => $order)
            <tr>
               <td> {{ $key+1 }}</td>
               <td> {{ $order -> id}} </td>
               <td> {{ $order -> name}}</td>
               <td> {{ $order -> address}}</td>
               <td> {{ $order -> phone}}</td>
               <td> {{ $order -> status}}</td>
               <td style="display: flex;"> <a href="/be/products/{{ $order->id }}" class="btn btn-info btn-sm">show</a>
               <a href="/be/products/{{ $order->id }}/edit" class="btn btn-default btn-sm">edit</a>
               </td>
            </tr>  
         @empty
            <tr>
               <td class="text-center" colspan="4">
                  No Data
               </td>
            </tr>
         @endforelse
      </tbody>
   </table>
   </div>
 </div>
    
@endsection
