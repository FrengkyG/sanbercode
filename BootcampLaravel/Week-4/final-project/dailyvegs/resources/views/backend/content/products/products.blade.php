@extends('backend.layouts.master')

@section('content')
<div class="card shadow mb-4">
   <div class="card-header py-3">
     <h3 class="m-0 font-weight-bold text-primary">Product List</h3>
   </div>
   <div class="card-body">
      @if(session('success'))
      <div class="alert alert-success" id="popup-success">
         {{ session('success')}}
      </div>
   @endif
   <a href="{{ route('products.create') }}" class="btn btn-primary btn-icon-split mb-2">
      <span class="icon text-white-50">
         <i class="fas fa-plus-circle"></i>
      </span>
      <span class="text">Create New Product</span>
    </a>
   
   <table class="table table-bordered">
      <thead>
         <tr>
            <th style="width: 10px">#</th>
            <th>Name</th>
            <th>Price</th>
            <th>Image</th>
            <th style="width: 40px">Actions</th>
         </tr>
      </thead>
      <tbody>
         @forelse ($products as $key => $product)
            <tr>
               <td> {{ $key+1 }}</td>
               <td> {{ $product -> product_name}} </td>
               <td> Rp {{ number_format($product -> price,2)}}</td>
               <td> {{ $product -> image}}</td>
               <td style="display: flex;"> <a href="/be/products/{{ $product->id }}" class="btn btn-info btn-sm">show</a>
               <a href="/be/products/{{ $product->id }}/edit" class="btn btn-default btn-sm">edit</a>
               <form action="/be/products/{{ $product->id }}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="delete" class="btn btn-danger btn-sm">
               </form>
               </td>
            </tr>  
         @empty
            <tr>
               <td class="text-center" colspan="4">
                  No Data
               </td>
            </tr>
         @endforelse
      </tbody>
   </table>
   </div>
 </div>
    
@endsection
