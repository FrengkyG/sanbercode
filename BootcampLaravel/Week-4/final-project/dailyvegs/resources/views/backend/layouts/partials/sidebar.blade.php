<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

   <!-- Sidebar - Brand -->
   <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/be/">
     <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-leaf"></i>
     </div>
     <div class="sidebar-brand-text mx-3">DailyVegs</div>
   </a>

   <!-- Divider -->
   <hr class="sidebar-divider my-0">

   <!-- Divider -->
   <hr class="sidebar-divider">

   <!-- Heading -->
   <div class="sidebar-heading">
     Interface
   </div>

   <li class="nav-item">
      <a class="nav-link" href="{{route('products.index')}}">
         <i class="fas fa-database"></i>
         <span>Products</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{route('orders.index')}}">
         <i class="fas fa-receipt"></i>
        <span>Orders</span></a>
    </li>

 </ul>