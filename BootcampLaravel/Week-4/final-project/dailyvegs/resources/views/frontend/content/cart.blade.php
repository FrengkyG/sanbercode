@extends('frontend.layouts.master')

@section('content')
<div class="hero-wrap hero-bread" style="background-image: url('{{ asset('/vegefoods/images/bg_1.jpg') }}');">
   <div class="container">
     <div class="row no-gutters slider-text align-items-center justify-content-center">
       <div class="col-md-9 ftco-animate text-center">
          <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Cart</span></p>
         <h1 class="mb-0 bread">My Cart</h1>
       </div>
     </div>
   </div>
 </div>
 <section class="ftco-section ftco-cart">
   <div class="container">
      <div class="row">       
       <div class="col-md-12 ftco-animate">
          <div class="cart-list">
             <table class="table">
                <thead class="thead-primary">
                  <tr class="text-center">
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>Product name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    {{-- <th>Total</th> --}}
                  </tr>
                </thead>
                <tbody>
                  @if(session('success'))
                     <div class="alert alert-success" id="popup-success">
                        {{ session('success')}}
                     </div>
                  @endif
                  @foreach ($carts as $cart)
                  <tr class="text-center">
                        <td class="product-remove">
                           <form action="/cart/{{ $cart->id }}" method="post">
                              @csrf
                              @method('DELETE')
                           {{-- <a href="/cart/{{ $cart->id }}/delete"><span class="ion-ios-close"></span></a> --}}
                           <input type="submit" value="x" class="ion-ios-close">
                           </form>
                        </td>
                        
                     <td class="image-prod"><div class="img" style="background-image:url({{asset('/vegefoods/images/product-1.jpg')}});"></div></td>
                     
                     <td class="product-name">
                        <h3>{{$cart->product_name}}</h3>
                     </td>
                     
                     <td class="price" id="price">Rp {{number_format($cart->price,2)}}</td>
                     
                     <td class="quantity">
                        <div class="input-group mb-3">
                          <input type="text" id="qty" name="quantity" class="quantity form-control input-number" value="1" min="1" max="100" disabled>
                       </div>
                     </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
           </div>
       </div>
    </div>
    <form action="/cart" class="info" method="POST">
      @csrf
    <div class="row justify-content-end">
       <div class="col-lg-8 mt-5 cart-wrap ftco-animate">
          <div class="cart-total mb-3">
             <h3>Shipping Information</h3>
             <p>Enter your destination</p>
              
           <div class="form-group">
              <label for="name">Name</label>
             <input type="text" name="name" class="form-control text-left px-3" placeholder="">
           </div>
           <div class="form-group">
              <label for="address">Address</label>
             <input type="text" name="address" class="form-control text-left px-3" placeholder="">
           </div>
           <div class="form-group">
              <label for="phone">Phone Number</label>
             <input type="text" name="phone" class="form-control text-left px-3" placeholder="">
           </div>
         
         </div>
       </div>
       <div class="col-lg-4 mt-5 cart-wrap ftco-animate">
          <div class="cart-total mb-3">
             <h3>Cart Totals</h3>
             <p class="d-flex total-price">
                  <span>Total</span>
                  <span id="total-full" name="total" class="font-weight-bold">Rp {{number_format($total,2)}}</span>
             </p>
             {{-- <p><a href="/cart/" class="btn btn-primary py-3 px-4">Checkout</a></p> --}}
             <button type="submit" class="btn btn-primary py-3 px-4">Checkout</button>
             {{-- TODO :change button --}}
          </div>
       </div>
      </form>
   </div>
   </div>
</section>
@endsection