@extends('frontend.layouts.master')

@section('content')
<div class="hero-wrap hero-bread" style="background-image: url('{{ asset('/vegefoods/images/bg_1.jpg') }}');">
   <div class="container">
     <div class="row no-gutters slider-text align-items-center justify-content-center">
       <div class="col-md-9 ftco-animate text-center">
          <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Products</span></p>
         <h1 class="mb-0 bread">Products</h1>
       </div>
     </div>
   </div>
 </div>

 <section class="ftco-section">
    <div class="container">
       <div class="row">
         @foreach ($products as $product)
 
           <div class="col-md-6 col-lg-3 ftco-animate">
             <div class="product">
               <a href="#" class="img-prod"><img class="img-fluid" src="{{ asset('vegefoods/images/product-1.jpg') }}" alt="Colorlib Template">
                 <div class="overlay"></div>
               </a>
               <div class="text py-3 pb-4 px-3 text-center">
                 <h3><a href="#">{{$product -> product_name}}</a></h3>
                 <div class="d-flex">
                   <div class="pricing">
                   <p class="price"><span>Rp {{number_format($product -> price,2)}}</span></p>
                   </div>
                 </div>
                 <div class="bottom-area d-flex px-3">
                   <div class="m-auto d-flex">
                     <a href="/cart/{{$product -> id}}" class="buy-now d-flex justify-content-center align-items-center mx-1">
                       <span><i class="ion-ios-cart"></i></span>
                     </a>
                   </div>
                 </div>
               </div>
             </div>
           </div>
           
       @endforeach
       </div>
    </div>
</section>
@endsection