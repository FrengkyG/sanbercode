@extends('frontend.layouts.master')

@section('content')

<section id="home-section" class="hero">
   <div class="home-slider owl-carousel">
    <div class="slider-item" style="background-image: url( {{ asset('/vegefoods/images/bg_1.jpg') }});">
       <div class="overlay"></div>
      <div class="container">
        <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

          <div class="col-md-12 ftco-animate text-center">
            <h1 class="mb-2">We serve Fresh Vegetables &amp; Fruits</h1>
            <h2 class="subheading mb-4">We deliver organic vegetables &amp; fruits</h2>
          </div>

        </div>
      </div>
    </div>
    <div class="slider-item" style="background-image: url( {{ asset('/vegefoods/images/bg_2.jpg') }});">
       <div class="overlay"></div>
      <div class="container">
        <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

          <div class="col-sm-12 ftco-animate text-center">
            <h1 class="mb-2">100% Fresh &amp; Organic Foods</h1>
            <h2 class="subheading mb-4">We deliver organic vegetables &amp; fruits</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@include('frontend.layouts.partials.info')

<section class="ftco-section">
  <div class="container">
    <div class="row justify-content-center mb-3 pb-3">
      <div class="col-md-12 heading-section text-center ftco-animate">
        <span class="subheading">Featured Products</span>
        <h2 class="mb-4">Our Products</h2>
        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
      </div>
    </div>   		
</div>
<!-- content untuk di for -->
  <div class="container">
    <div class="row">
      <?php $count = 0; ?>
      @foreach ($products as $product)
        <?php if($count == 8) break; ?>

          <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="product">
              <a href="#" class="img-prod"><img class="img-fluid" src="{{ asset('vegefoods/images/product-1.jpg') }}" alt="Colorlib Template">
                <div class="overlay"></div>
              </a>
              <div class="text py-3 pb-4 px-3 text-center">
                <h3><a href="#">{{$product -> product_name}}</a></h3>
                <div class="d-flex">
                  <div class="pricing">
                  <p class="price"><span>Rp {{number_format($product -> price,2)}}</span></p>
                  </div>
                </div>
                <div class="bottom-area d-flex px-3">
                  <div class="m-auto d-flex">
                    <a href="/cart/{{$product -> id}}" class="buy-now d-flex justify-content-center align-items-center mx-1">
                      <span><i class="ion-ios-cart"></i></span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

        <?php $count++; ?>
      @endforeach
      
    </div>
  </div>
</section>

@include('frontend.layouts.partials.testimony')
@include('frontend.layouts.partials.footer')
@endsection
