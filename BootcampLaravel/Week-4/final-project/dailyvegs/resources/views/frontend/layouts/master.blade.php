<!DOCTYPE html>
<html lang="en">
  <head>
    <title>DailyVegs</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    @include('frontend.layouts.partials.link')
  </head>
  	 @include('frontend.layouts.partials.navbar')
    
	 @yield('content')

  <script src="{{ asset('/vegefoods/js/jquery.min.js') }}"></script>
  <script src="{{ asset('/vegefoods/js/jquery-migrate-3.0.1.min.js') }}"></script>
  <script src="{{ asset('/vegefoods/js/popper.min.js') }}"></script>
  <script src="{{ asset('/vegefoods/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('/vegefoods/js/jquery.easing.1.3.js') }}"></script>
  <script src="{{ asset('/vegefoods/js/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('/vegefoods/js/jquery.stellar.min.js') }}"></script>
  <script src="{{ asset('/vegefoods/js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('/vegefoods/js/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('/vegefoods/js/aos.js') }}"></script>
  <script src="{{ asset('/vegefoods/js/jquery.animateNumber.min.js') }}"></script>
  <script src="{{ asset('/vegefoods/js/bootstrap-datepicker.js') }}"></script>
  <script src="{{ asset('/vegefoods/js/scrollax.min.js') }}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="{{ asset('/vegefoods/js/google-map.js') }}"></script>
  <script src="{{ asset('/vegefoods/js/main.js') }}"></script>
    
  </body>
</html>