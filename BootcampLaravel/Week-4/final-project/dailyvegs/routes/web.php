<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index');
Route::get('/shop', 'FrontendController@shop');
Route::get('/about', 'FrontendController@about');
Route::get('/cart', 'FrontendController@cart');
Route::delete('/cart/{id}', 'OrdersController@destroy');
Route::post('/cart', 'OrdersController@store');
Route::get('/cart/{id}', 'FrontendController@cartshow');
Route::get('/be', function () {
    return view('backend.layouts.master');
});

Route::resource('/be/products', 'ProductsController');
Route::resource('/be/orders', 'OrdersController');
