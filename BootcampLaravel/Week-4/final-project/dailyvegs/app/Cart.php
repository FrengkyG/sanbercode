<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = "carts";
    protected $guarded = [];

    public function products(){
        return $this->hasMany('App\Product', 'product_id');
    }
}
