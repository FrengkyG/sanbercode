<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Order;
use App\Cart;

class OrdersController extends Controller
{
    public function index()
    {
        $orders = Order::all();
        return view('backend.content.orders.orders', compact('orders'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required'
        ]);

        // dd($request->all());

        $orders = new Order;
        $orders->qty = 1;
        $orders->total = DB::table('carts')
                        ->join('products', 'carts.product_id', '=', 'products.id')
                        ->sum('products.price');
        $orders->name = $request["name"];
        $orders->address = $request["address"];
        $orders->phone = $request["phone"];
        $orders->status = "Belum Dikirim";
        $orders->user_id = 1;
        $orders->save();
        DB::table('carts')->delete();
        return redirect('/cart')->with('success', 'Order telah diterima');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        Cart::destroy($id);
        return redirect('/cart')->with('success', 'Product berhasil dihapus');
    }
}
