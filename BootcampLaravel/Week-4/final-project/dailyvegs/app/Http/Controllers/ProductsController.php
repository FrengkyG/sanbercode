<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;

class ProductsController extends Controller
{
    public function index()
    {
        // $products = DB::table('products')->get();
        $products = Product::all();
        return view('backend.content.products.products', compact('products'));
    }

    public function create()
    {
        return view('backend.content.products.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'product_name' => 'required|unique:products',
            'price' => 'required|numeric'
        ]);
        
        $product = new Product;
        $product->product_name = $request["product_name"];
        $product->price = $request["price"];
        $product->save();
        return redirect('/be/products')->with('success', 'Product berhasil dibuat');
    }

    public function show($id)
    {
        $product = Product::find($id);
        return view('backend.content.products.show', compact('product'));
    }

    public function edit($id)
    {
        $product = Product::find($id);
        return view('backend.content.products.edit', compact('product'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'product_name' => 'required|unique:products',
            'price' => 'required|numeric'
        ]);
        
        $update = Product::where('id', $id)->update([
            "product_name" => $request["product_name"],
            "price" => $request["price"]
        ]);

        return redirect('/be/products')->with('success', 'Product berhasil diupdate');
    }

    public function destroy($id)
    {
        Product::destroy($id);
        return redirect('/be/products')->with('success', 'Product berhasil dihapus');
    }
}
