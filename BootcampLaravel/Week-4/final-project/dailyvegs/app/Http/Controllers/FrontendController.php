<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;
use App\Cart;


class FrontendController extends Controller
{
    public function index(){
        $products = DB::table('products')->get();
        return view('frontend.content.index', compact('products'));
    }

    public function shop(){
        $products = DB::table('products')->get();
        return view('frontend.content.shop', compact('products'));
    }

    public function about(){
        return view('frontend.content.about');
    }

    public function cart(){
        $carts = DB::table('carts')
        ->join('products', 'carts.product_id', '=', 'products.id')
        ->select('carts.id', 'carts.product_id', 'products.product_name', 'products.price')
        ->get();
        
        // $user = Cart::where('user_id', 1)->get();
        // // dd($user->all());
        // $carts = $user->products();

        // dd($carts->all());
        $total = DB::table('carts')
        ->join('products', 'carts.product_id', '=', 'products.id')
        ->sum('products.price');
        return view('frontend.content.cart', compact('carts', 'total'));
        dd($carts->all());
    }

    public function cartshow($id){
        $cart = new Cart;
        $cart->user_id = 1;
        $cart->product_id = $id;
        $cart->save();
        return redirect('/cart');
    }
}
